//
//  LoginViewController.swift
//  SensorStreamming
//
//  Created by Shalva Avanashvili on 04/11/2017.
//  Copyright © 2017 Shalva Avanashvili. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController : BaseViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var instanceUrlTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEBUG
            userNameTextField.text = "madpdemo"
            passwordTextField.text = "madp2017"
            instanceUrlTextField.text = "https://iot.cs.ut.ee"
        #endif
        
        navigationItem.title = "Login"
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        guard let userName = userNameTextField.text, let instanceUrl = instanceUrlTextField.text, let password = passwordTextField.text else {
            showError(text: Localisation.local("error.gneric"))
            return
        }
        
        if userName.isEmpty || instanceUrl.isEmpty || password.isEmpty {
            showError(text: Localisation.local("login.fields.should.be.filled"))
            return
        }
        
        //assigning values to our settings
        Settings.UserName = userName
        Settings.Password = password
        Settings.InstanceUrl = instanceUrl
        
        //Checking credentials
        Session.credentialsCorrect() {
            (correct: Bool) in
            
            if correct {
                //open login view
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarController") else {
                    print("Unable to instantiate viewcontroller: MainTabBarController")
                    return
                }
                UIApplication.shared.keyWindow?.rootViewController = vc
            } else {
                //show alert box indicating error
                self.showError(text: Localisation.local("login.credentials.not.correct"))
            }
        }
    }
}

