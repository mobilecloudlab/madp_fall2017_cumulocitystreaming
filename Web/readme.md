Launch a local HTTP server in the Web directory to launch the web application.

If Python is installed, a quick way is to run "py -m SimpleHTTPServer 8000" in the directory and to navigate to https://localhost:8080.