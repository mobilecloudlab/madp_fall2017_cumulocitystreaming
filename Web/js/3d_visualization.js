window.addEventListener( 'resize', _onWindowResizeWithDelay, false );

//The default rotation mode of the 3D visualization
rotationMode = "PITCH";

$(document).ready(function() {
  $("#rotation-mode-p").click(function() {
      rotationMode = "PITCH";
  });
  $("#rotation-mode-r").click(function() {
      rotationMode = "ROLL";
  });

});

function _onWindowResizeWithDelay() {
  //Update the 3D size with a small delay since the canvas size is calculated from the chart sizes and the charts're not updated immediately after resize
  window.setInterval(function(){
      onWindowResizeFor3D();
  }, 200 );
}

//called whenever new data is received by any sensor
//only the latest measurement from that query is passed
function onSensorUpdated(sensorName, measurements) {
  if(sensorName == "c8y_Rotation") {
    var p = measurements[0];
    var y = measurements[1];
    var r = measurements[2];

    p = THREE.Math.degToRad(p);
    y = THREE.Math.degToRad(y + 90);
    r = THREE.Math.degToRad(r);

   if(rotationMode == "PITCH") {
      phone.rotation.x = 0;
      phone.rotation.y = Math.PI;
      phone.rotation.z = p + Math.PI/2;
    } else if(rotationMode == "ROLL") {
      phone.rotation.x = r;
      phone.rotation.y = Math.PI;
      phone.rotation.z = 0;
    }
    //Position the north direction indicator based on the yaw(azimuth) measurement
    northArrow.setDirection(new THREE.Vector3(Math.sin(y), 0, Math.cos(y)).normalize());
  }
}

function onWindowResizeFor3D(){
  //Resize the 3D canvas depending on the size of the rotation chart
  //Reasoning being the rotation chart is automatically updated with bootstrap but the 3D canvasd is not.
  var bb = document.querySelector ('#c8y_Rotation_chart')
                    .getBoundingClientRect();
  var width = (bb.right - bb.left) - 10
  var height = (bb.bottom - bb.top) - 10;

  camera.aspect = width / height;
  camera.updateProjectionMatrix();

  renderer.setSize( width, height );
}

function setup3D() {
  camera = new THREE.PerspectiveCamera( 70, 1, 1, 5000 );

  setupScene();
        
  renderer = new THREE.WebGLRenderer();
  renderer.setClearColor( 0xdddddd )
  renderer.setPixelRatio( 1 );
  document.getElementById("3D").appendChild( renderer.domElement );

  //Trigger resizing once to resize the canvas to current correct size
  onWindowResizeFor3D();

  //Add controls for camera movement around the phone model
  controls = new THREE.OrbitControls( camera, renderer.domElement );
  controls.enableDamping = true;
  controls.dampingFactor = 0.25;
  controls.enablePan = false;
  controls.enableZoom = true;
  controls.minDistance = 10;
  controls.maxDistance = 35;

  //Initial camera position
  var angle = 5;
        var cameraRadius = 27
  camera.position.set(Math.sin(angle) * cameraRadius, cameraRadius * 0.3 - 2, Math.cos(angle) * cameraRadius);
  camera.lookAt(new THREE.Vector3(0, -2, 0));

  animate();
}

function setupScene() {
  scene = new THREE.Scene();
  var ambientLight = new THREE.AmbientLight(0x999999);
  scene.add(ambientLight);

  //Add an indicator of axes for assistance
  var axisHelper = new THREE.AxisHelper(5);
  axisHelper.position.set(-3.5, -14.5, -8.5);
  scene.add(axisHelper);

  //Load the phone model and add it to the scene
  var loader = new THREE.FBXLoader();
    // load a resource
    loader.load(
      // resource URL
      'models/phone.fbx',
      // Function when resource is loaded
      function ( object ) {
        phone = object;
        object.scale.set(2, 2, 2);
        scene.add( object );
      }
    );

    //Initialize the north arrow pointer
    var dir = new THREE.Vector3( 1, 0, 0 ).normalize();
    var origin = new THREE.Vector3( 0, 3, 0 );
    var length = 15;
    var hex = 0x000000;
    northArrow = new THREE.ArrowHelper( dir, origin, length, hex, length * 0.5);
    scene.add( northArrow );
}


function animate() {
  //Render loop
  requestAnimationFrame( animate );
  controls.update();
  renderer.render( scene, camera );
}
