$(document).ready(function() {
  validateURLParameters();

  setupInitialGraphs();

  setup3D();
});


function validateURLParameters() {
  url = $.urlParam('url');
  deviceId = $.urlParam('device')
  token = $.urlParam('token');

  //Display an error alert when any of the required URL parameters are missing
  if(url == null || deviceId == null || token == null) {
    $("#param-alert").removeClass("hidden");
  }
}

function sendOperation(operation) {
  body = {
    "deviceId" : deviceId
  };
  body[operation] = {};
  console.log("Sending operation " + operation);

  var site = url + "/devicecontrol/operations";
  $.ajax
    ({
      type: "POST",
      url: site,
      contentType: "application/json",
      data: JSON.stringify(body),
      async: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", token);
      },
      success: function (data){
      },
      error: function() {
      }
  });
}

function setupInitialGraphs() {
  addSensor("c8y_Rotation", ["p", "a", "r"], 1, 20, -180, 180, false);
  addSensor("c8y_Acceleration", ["x", "y", "z"], 1, 20, -10, 10, true);
  addSensor("c8y_Gyroscope", ["x", "y", "z"], 1, 20, -5, 5, true);
}

function addSensor(name, valueNames, updateFrequencySec, pastSeconds, minValue, maxValue, deletable) {
  var sensor = new Sensor(name, valueNames, minValue, maxValue, deletable);
  sensor.createChart();
  sensor.start(updateFrequencySec, pastSeconds);
}

function onAddSensorClicked() {
  //Hide the sensor adding alert if it was visible from earlier attempt
  var alert = $("#add-sensor-alert");
  alert.addClass("hidden");
  //Hide the error text if there was one from earlier
  var errorText = $("#add-sensor-error-message");
  errorText.empty();

  //Validate fields and if any have issues, return.
  var name = $("#newSensorName").val();
  if(!name) {
    console.log("Name is invalid");
    errorText.append("Invalid sensor name. ");
    alert.removeClass("hidden");
    return;
  }
  if($('#' + name + '_chart').length) {
    console.log("Sensor already exists");
    errorText.append("A sensor with that name already exists. ");
    alert.removeClass("hidden");
    return;
  }

  var minValue = Number($("#newSensorMinValue").val());
  if(!minValue) {
    console.log("Min value is invalid");
    errorText.append("Invalid min value. ");
    alert.removeClass("hidden");
    return;
  }
  var maxValue = Number($("#newSensorMaxValue").val());
  if(!maxValue) {
    console.log("Max value is invalid");
    errorText.append("Invalid max value. ");
    alert.removeClass("hidden");
    return;
  }

  if(minValue >= maxValue) {
    console.log("Max value is smaller tham min value");
    errorText.append("Min value can't be bigger than max value. ");
    alert.removeClass("hidden");
    return;
  }

  var updateInterval = Number($("#newSensorUpdateInterval").val());
  if(!updateInterval) {
    console.log("Update interval is invalid");
    errorText.append("Invalid update interval. ");
    alert.removeClass("hidden");
    return;
  }

  var valueNames = $("#newSensorValueNames").val();
    if(!valueNames) {
    console.log("Value names are invalid");
    errorText.append("Invalid value names. ");
    alert.removeClass("hidden");
    return;
  }
  valueNames = valueNames.split(",");
  for(i = 0; i < valueNames.length; i++) {
    valueNames[i] = valueNames[i].trim();
  }

  //If values are acceptable, add a new sensor
  addSensor(name, valueNames, updateInterval, 20, minValue, maxValue, true);
}
