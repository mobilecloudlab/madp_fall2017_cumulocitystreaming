//Values used to mark lines on graphs. First value gets the first color etc.
CHART_COLORS = ["rgba(255, 0, 0, 255)", "rgba(0, 255, 0 ,255)", "rgba(0, 0, 255, 255)"
, "rgba(255, 0, 255, 255)", "rgba(0, 255, 255, 255)", "rgba(255, 255, 0, 255)", "rgba(0, 0, 0, 255)"];

//Values used to mark lines on graphs. First value gets the first color etc.
CHART_BACKGROUND_COLORS = ["rgba(255, 50, 50, 255)", "rgba(50, 255, 50 ,255)", "rgba(50, 50, 255, 255)"
, "rgba(255, 50, 255, 255)", "rgba(50, 255, 255, 255)", "rgba(255, 255, 50, 255)", "rgba(50, 50, 50, 255)"];

//A class for adding a sensor
function Sensor(name, valueNames, min, max, deletable) {
  if(name instanceof String) {
    console.log("Error: name must be a string");
  }
  if(!valueNames instanceof Array) {
    console.log("Error: valueNames must be an array of strings");
  }
  if(!min instanceof Number) {
    console.log("Error: min must be a number");
  }
  if(!max instanceof Number) {
    console.log("Error: max must be a number");
  }
  if(!deletable instanceof Boolean) {
    console.log("Error: deletable must be a boolean");
  }
  if(min >= max) {
    console.log("Error: min must be smaller than max.");
  }
  this.name = name;
  this.valueNames = valueNames;
  this.min = min;
  this.max = max;
  this.deletable = deletable;
}

//Whenever chart time interval is changed, change the class pastSeconds interval and force an update
Sensor.prototype.onChartTimeIntervalChanged = function() {
      var interval = $("#" + this.name + "-interval").val();
      var unit = $("#" + this.name + "-unit").val();
      console.log(interval + " " + unit);

      if(unit == "Seconds") {
        this.pastSeconds = parseInt(interval);
        this.lastUpdate = null;
      } else {
        this.pastSeconds = parseInt(interval) * 60;
        this.lastUpdate = null;
      }

}

Sensor.prototype.createChart = function () {
  console.log("Creating a chart for " + this.name);

  //Create a HTML element with a canvas for the chart
  var parent = $("#chartsDiv");
  parent.append(this._generateHTML());

  //Trigger an event whenever the time settings are changed
  var s = this;
  $("#" + this.name + "-interval").change(function() {
      s.onChartTimeIntervalChanged();
  });

  $("#" + this.name + "-unit").change(function() {
      s.onChartTimeIntervalChanged();
  });

  //If this sensor has a remove button, it can be clicked to remove itself
  $("#" + this.name + "-remove").click(function() {
      s.remove();
  });


  //Initialize data sets for the chart
  var datasets = [];
  for(i = 0; i < this.valueNames.length; i++) {
    var dataset = {
      label: this.valueNames[i],
      fill: false,
      borderColor: CHART_COLORS[i],
      backgroundColor: CHART_BACKGROUND_COLORS[i],
      data: [0]
    };
    datasets.push(dataset);
  }

  //Initialize rest of the chart
  this.chartContext = document.getElementById(this.name + "_chart").getContext("2d");
  this.chartData = {
    labels: [moment().subtract({seconds:20})],
    datasets: datasets
  };
  this.chartOptions = {
  animation: false,
  scales: {
        xAxes: [{
          maxRotation: 0,
          type: "time",
          time: {
            parser: 'HH:mm:ss',
            unit: 'second',
            tooltipFormat: 'HH:mm:ss',
            displayFormats: {
                        second: 'mm:ss'
                    }
          },
        }, ],
        yAxes: [{
            ticks: {
                min: this.min,
                max: this.max
            }
        }]
      },
  };
  this.chart = new Chart(this.chartContext, {
    type: "line",
    data: this.chartData,
    options: this.chartOptions,
  });
}

Sensor.prototype.remove = function() {
  console.log("Removing chart for " + this.name);
  clearInterval(this.interval);
  $("#" + this.name + "-parent").remove();
}

Sensor.prototype._generateHTML = function() {
    var html = 
    '<div id="' + this.name + '-parent" class="col-lg-6 col-sm-12">' + 
    ' <div class="row">' + 
    '   <div class="col-lg-6 col-sm-12">' + 
    '     <h3>' + this.name + '</h3>' + 
    '   </div>' + 
    '   <div class="col-lg-6 col-sm-12 period-select">' + 
    '     Last' + 
    '     <input id="' + this.name + '-interval" class="form-control" type="number" value="20">' + 
    //'     Seconds' +
    '     <div class="form-group">' + 
    '       <select id="' + this.name + '-unit" disabled class="form-control">' + 
    '         <option value="Seconds">Seconds</option>' + 
    '         <option value="Minutes">Minutes</option>' + 
    '       </select>' + 
    '     </div>' + 
    (this.deletable ? '<span id="'+ this.name + '-remove" class="glyphicon glyphicon-remove chart-remove pull-right"></span>' : '') +
    '   </div>' + 
    ' </div>' + 
    ' <canvas class="chart-canvas" id="' + this.name + '_chart"></canvas>' + 
    '</div>';
    return html;
}

//Triggers updating of the chart at the defined interval
Sensor.prototype.start = function (updateFrequencySec, pastSeconds) {
  if(!updateFrequencySec instanceof Number) {
    console.log("Error: updateFrequencySec must be a number");
  }
    if(!pastSeconds instanceof Number) {
    console.log("Error: pastSeconds must be a number");
  }
  this.updateFrequencySec = updateFrequencySec;
  this.pastSeconds = pastSeconds;
  this.lastUpdate = null;
  var t = this;
  this.interval = window.setInterval(function(){
      t._update();
  }, updateFrequencySec * 1000.0 );
}

Sensor.prototype._update = function() {
  var minimumTime = moment().subtract({seconds:this.pastSeconds});
  //Set start time to the minimal timestamp to fetch measurements from
  if(this.lastUpdate != null) {
    //A) chart has been updated before, doesn't necessarily have to fetch last X seconds
    //only since last measurement
    var start = processDate(this.lastUpdate.format());
  } else {
    //The last measurement was so far back that we need to query the whole interval
    //set for this chart
    var start = processDate(minimumTime.format());
  }
  var end = processDate(moment().format());

  //Build a request URL for the measurements
  var site = url + "/measurement/measurements?source=" + deviceId + 
    "&dateFrom=" + start + 
    "&dateTo=" + end + "&fragmentType=" + this.name + "&pageSize=2000";

  var sensor = this;

  $.ajax
    ({
      type: "GET",
      url: site,
      async: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", token);
      },
      success: function (data){
          sensor._onNewDataReceived(data);
      },
      error: function() {
        console.log("Failed to fetch data.");
      }
  });
}

Sensor.prototype._onNewDataReceived = function(data) {
  var minimumTime = moment().subtract({seconds:this.pastSeconds});

  var measurements = data["measurements"];

  //Create a soft copy of the chart datasets for more convenient access
  var data = [];
  for(i = 0; i < this.valueNames.length; i++) {
    data.push(this.chartData.datasets[i].data);
  }

  //Create a soft copy of the chart labels for more convenient access
  var labels = this.chartData.labels;
  for(l in labels) {
    var label = labels[l];
    //For any measurements that are now beyond our time interval, delete
    //the corresponding data values and the label
    if(label.isBefore(minimumTime)) {
        labels.splice(l, 1);
        for(i = 0; i < this.valueNames.length; i++) {
          data[i].splice(l, 1);
        }
    }
  }

  var measurementValues = [];
  //Read in the new measurements
  for(id = 0; id < measurements.length; id++) {
    var measurement = measurements[id];
    var time = measurement["time"]
    labels.push(moment(time));

    if(!measurement[this.name]) {
      //Skip invalid measurements that might end up here in extreme cases
      //e.g. fetching sensor c8y_abc will also fetch sensors c8y_abcd
      continue;
    }

    var sensorMeasurement = measurement[this.name];
    for(i = 0; i < this.valueNames.length; i++) {
      var value = sensorMeasurement[this.valueNames[i]]["value"];
      data[i].push(value);
      if(id == measurements.length - 1) {
        measurementValues.push(value);
      }
    }

    //Trigger onSensorUpdated for each sensor with it's latest measurement
    //This is used by the rotation visualization
    if(id == measurements.length - 1) {
      onSensorUpdated(this.name, measurementValues);
      this.lastUpdate = moment(time);
    }

  }
  this.chart.update();
}