function onFetchDevicesClicked() {

  var url = "https://" + document.getElementById("url").value;
  var username = document.getElementById("username").value;
  var password = document.getElementById("pwd").value;
  connect(url, username, password);
}

function connect(url, username, password) {
  //Fetch all devices with type "c8y_SensorPhone"
  var token = "Basic " + btoa(username + ":" + password);
  $.ajax
    ({
      type: "GET",
      url: url + "/inventory/managedObjects/?pageSize=2000&type=c8y_SensorPhone",
      async: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", token);
      },
      success: function (data){
        //Prevents duplicate listing of devices if button was rapidly pressed
        if($("#login").hasClass("hidden") == false) {  
          console.log("Success");
          onSuccessfullyConnected(data, url, token);
        }
      },
      error: function() {
        console.log("Failure");
        $("#login-fail-alert").removeClass("hidden");
      }
  });
}

function onSuccessfullyConnected(data, url, token) {
    $("#login").addClass("hidden");
    $("#device_select").removeClass("hidden");

    var devices = data["managedObjects"];
      //Populate the list of devices with fetched objects
    for(i in devices) {
      var deviceData = devices[i];
      var name = deviceData["name"];
      if(name == null) {
          continue;
      }
      console.log(deviceData);
      var href = "/visualization.html?url=" + url + "&device=" + deviceData["id"] + "&token=" + token;
      var str = "[ID:" + deviceData["id"] + ", Created: " + deviceData["creationTime"] +", Type: " + deviceData["type"] + "] " + name;

      $("#devices-list").append('<a href="' + href + '" class="list-group-item">' + str + '</a>');
    }

}