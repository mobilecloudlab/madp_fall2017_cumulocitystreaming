package ut.madp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ut.madp.cumulocitysensorstreaming.CumulocityController;
import ut.madp.cumulocitysensorstreaming.CumulocityControllerListener;
import ut.madp.cumulocitysensorstreaming.CumulocitySensor;
import ut.madp.cumulocitysensorstreaming.CumulocitySensorImpl;

/**
 * Created by Madis-Karli Koppel on 05.03.2018.
 */


public class SensorService extends Service implements SensorEventListener{

    private static final String TAG = "Sensor_SERVICE";

    private int SENSOR_LISTENER_DELAY = 500000;

    private SensorManager mSensorManager;

    private Map<Integer, CumulocitySensor> cumulocitySensors;

    private CumulocityController mCumulocityController;

    private float[] mGravity;

    private float[] mGeomagnetic;

    private boolean haveShownFailedRegInfo = false;

    private boolean sendRotation = false;

    // Messengers for communicating with the service.
    final Messenger incomingMessenger = new Messenger(new IncomingHandler());
    private Messenger outgoingMessenger;


    // Register sensors in both cumulocity and local device
    // Sends smartrest template requests and uses it to send sensor data
    // Device itself registeres a sensormanager and sensors
    private void initSensors() {
        Log.i(TAG, "initSensors");
        //Beware: if your cumulocity controller is properly async
        //with device registration, this will not be called on the main thread
        //and might need special handling on your end

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //3. register sensors in Cumulocity and device
        cumulocitySensors = new HashMap<>();

        // For registering we need to give them a Cumulocity name, the data they send (x,y,z), min update time and
        // Android sensor type
        // Value 500 means that at minimum, the sensor sends data every 500 ms (0.5 seconds) but in reality it only sends every second
        addSensor("c8y_Acceleration", new String[] { "x",  "y", "z" }, 500, Sensor.TYPE_ACCELEROMETER);
        addSensor("c8y_Gyroscope", new String[] { "x",  "y", "z" }, 500, Sensor.TYPE_GYROSCOPE);
        addSensor("c8y_MagneticField", new String[] { "x",  "y", "z" }, 500, Sensor.TYPE_MAGNETIC_FIELD);

        // Rotation is a special case as it requires data from two sensors.
        addRotationSensor();
    }

    // Register sensors in Cumulocity and Android's sensormanager
    private CumulocitySensor addSensor(String sensorName, String[] namesOfValues, int minimalUpdateTimeMs, final int sensorInt) {
        Log.i(TAG, "addSensor");
        CumulocitySensor cumulocitySensor = new CumulocitySensorImpl();
        cumulocitySensor.register(mCumulocityController, sensorName, namesOfValues, minimalUpdateTimeMs);
        cumulocitySensors.put(sensorInt, cumulocitySensor);

        sendToActivity(2, sensorInt, sensorName);

        registerSensor(sensorInt);

        return cumulocitySensor;
    }

    private void registerSensor(int sensorInt){
        Sensor sensor = mSensorManager.getDefaultSensor(sensorInt);
        mSensorManager.registerListener(this, sensor, SENSOR_LISTENER_DELAY);
    }

    private void unRegisterSensor(int sensorInt){
        Sensor sensor = mSensorManager.getDefaultSensor(sensorInt);
        mSensorManager.unregisterListener(this, sensor);
    }

    // A special case example for rotationsensor
    // This is different as it does not listen to a sensor but collects data from two sensors
    // As such sensor manager is not used here BUT we still register it in Cumulocity
    private CumulocitySensor addRotationSensor(){
        CumulocitySensor cumulocitySensor = new CumulocitySensorImpl();
        cumulocitySensor.register(mCumulocityController, "c8y_Rotation", new String[] { "p",  "a", "r" }, 500);
        cumulocitySensors.put(9000, cumulocitySensor);

        // A button for turning sending data on and off
        sendToActivity(2, 99, "c8y_Rotation");

        sendRotation = true;

        return cumulocitySensor;
    }

    private void handleRotationUpdate(SensorEvent event){
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;

        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;

        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];

            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                // orientation contains: azimut, pitch and roll
                double azimuth = Math.toDegrees(orientation[0]);
                double pitch = Math.toDegrees(orientation[1]);
                double roll = Math.toDegrees(orientation[2]);

                CumulocitySensor rotationSensor = cumulocitySensors.get(9000);
                rotationSensor.pushNewData(new String[] { String.valueOf(pitch), String.valueOf(azimuth), String.valueOf(roll) });
            } else{
                Log.e(TAG, "rotation failed");
            }
            mGravity = null;
            mGeomagnetic = null;
        }
    }

    public void startService() {
        Log.i(TAG, "Sensor Service Intent started");
        // Request device hardware serial
        // It is used for requesting device credentials and to identify the device
        final String deviceId = Build.SERIAL;

        // Initialise Cumulocity controller
        String appVersion = "UNK";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Log.i(TAG, "Sensor Service started");

        mCumulocityController = new CumulocityController(this, appVersion);

        // 1. start listening to controller events
        Runnable cumulocityControllerListenerRunnable = new Runnable() {
            @Override
            public void run() {
                mCumulocityController.addListener(new CumulocityControllerListener() {
                    // When we receive the message onDeviceRegistered
                    // then we initialise sensor listeners
                    // and operations listeners
                    @Override
                    public void onDeviceRegistered() {
                        //Beware: if your cumulocity controller is properly async
                        //with device registration, this will not be called on the main thread
                        //and might need special handling on your end

                        Handler mainHandler = new Handler(Looper.getMainLooper());

                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                sendToActivity(1, -1, "Device registered, starting sensors and operations");
                            }
                        };
                        mainHandler.post(myRunnable);

                        initSensors();

                        mCumulocityController.initOperationListener(Arrays.asList(new String[]{"c8y_Restart", "c8y_Vibrate"}));
                    }

                    // When there is an error then we show a message to the user
                    @Override
                    public void onDeviceRegistrationError(final String errorMessage) {
                        Log.e(TAG, "BAD REG " + errorMessage);

                        if (!haveShownFailedRegInfo) {
                            haveShownFailedRegInfo = true;
                            Handler mainHandler = new Handler(Looper.getMainLooper());

                            Runnable myRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    sendToActivity(1, -1, errorMessage);
                                }
                            };
                            mainHandler.post(myRunnable);
                        }
                    }

                    // When we receive an operation we should perform it
                    // As an example we perform vibrate operation for restart and vibrate operations
                    // Operations are registered in onDeviceRegistered
                    @Override
                    public void onOperation(final String operation) {
                        Log.i(TAG, "created operation " + operation);

                        // Send message to activity to update log with operation received
                        Handler mainHandler = new Handler(Looper.getMainLooper());

                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                sendToActivity(1, -1, "Received operation " + operation);
                            }
                        };
                        mainHandler.post(myRunnable);

                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(1000);
                    }

                });
            }
        };

        cumulocityControllerListenerRunnable.run();

        //2. start the controller by first registering the device
        // this sends messages to listeners defined above
        mCumulocityController.registerDevice(deviceId);

    }


    // As this Activity also implements SensorEventListener then we can handle sensor changes here
    // Method that handles the onSensorChanged event and then uses CumulocitySensorImpl to send data to Cumuloctiy
    @Override
    public void onSensorChanged(SensorEvent event) {
        int type = event.sensor.getType();

        // TODO go over this with jakob
        // Format the data into 3 digit precision as more is not really needed
        // The reason is that sometimes Android also sends data in scientific notation (1e6) that is not understood by Cumulocity
        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));;
        df.setMaximumFractionDigits(3);

        // If we are dealing with regular sensors then the flow is easy
        CumulocitySensor sensor = cumulocitySensors.get(type);
        sensor.pushNewData(new String[] { df.format(event.values[0]), df.format(event.values[1]), df.format(event.values[2]) });

        // Rotation is special case - it needs data from two sensors that must be combined and converted
        if(sendRotation) handleRotationUpdate(event);
    }

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                // register and unregister sensors
                case 3:
                    // rotation sensor is different
                    if (msg.arg1 == 99){
                        sendRotation = (boolean) msg.obj;
                    }
                    // Other than rotation sensors, sorry for arrowhead
                    else{
                        if ((boolean) msg.obj){
                            registerSensor(msg.arg1);
                        } else{
                            unRegisterSensor(msg.arg1);
                        }
                    }

                    break;
                default:
                    Log.e(TAG, "Received unknown message " + msg.what);
                    super.handleMessage(msg);
            }
        }
    }

    public void sendToActivity(int what, int arg1, Object body) {
        Log.i(TAG, "Sending message " + what);
        Message message = Message.obtain();

        switch (what) {
            // send log updates
            case 1 :
                message.what = 1;
                message.obj = body;
                break;
            // Send sensor buttons
            case 2 :
                message.what = 2;
                message.arg1 = arg1;
                message.obj = body;
                break;
            // Send register/unregister Sensor message from sensor switch
            case 3:
                break;
            default :
                Log.e(TAG, "Attempting to send unknown message");
        }
        try {
            outgoingMessenger.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        Bundle extras = intent.getExtras();
        outgoingMessenger = (Messenger) extras.get("MESSENGER");

        startService();

        return incomingMessenger.getBinder();
    }

    @Override
    public void onCreate() {}

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
