package ut.madp.cumulocitysensorstreaming;

/**
 * Created by Madis-Karli Koppel on 20.11.2017.
 * Uses CumulocitySensor interface for handling sensors
 * Steps covered:
 * Registering sensor (and Smartrest registering sensor template)
 * Sending sensor data to cumulocity
 */

public class CumulocitySensorImpl implements CumulocitySensor {

    protected CumulocityController controller;

    protected String sensorName;

    protected String[] namesOfValues;

    protected int minimalUpdateTimeMs;

    protected long lastUpdateTime;

    protected String templateId;

    // Register a sensor
    // Set field names and request SmartREST templateId (X-id) from controller
    // SmartREST templateId is the same for all applications using same type of sensor
    // Controller generates templateID based on sensor name and application version
    @Override
    public void register(final CumulocityController controller, String sensorName, String[] namesOfValues,
                         int minimalUpdateTimeMs) {
        this.controller = controller;
        this.sensorName = sensorName;
        this.namesOfValues = namesOfValues;
        this.minimalUpdateTimeMs = minimalUpdateTimeMs;
        lastUpdateTime = 0;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                templateId = controller.registerSensor(CumulocitySensorImpl.this);
            }

        };
        runnable.run();
    }

    // Send sensor data to Cumulocity
    // DO note that it has its own update interval
    // This is because Android's sensor manager does not respect the minimalupdatetime
    // In our tests it sent data 20 times per second, when we requested it 2 times per second
    @Override
    public void pushNewData(String[] data) {

        //Ignore pushed data if at least minimal time has not passed since last update
        if(System.currentTimeMillis() - minimalUpdateTimeMs >= lastUpdateTime) {
            controller.sendSensorData(this, data);
            lastUpdateTime = System.currentTimeMillis();
        }
    }

    @Override
    public String getSensorName() {
        return sensorName;
    }

    @Override
    public String[] getNamesOfValues() {
        return namesOfValues;
    }

    @Override
    public String getTemplateId() {
        return templateId;
    }

}

