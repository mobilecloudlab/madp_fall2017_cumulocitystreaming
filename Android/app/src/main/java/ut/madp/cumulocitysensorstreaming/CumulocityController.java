package ut.madp.cumulocitysensorstreaming;

/**
 * Created by Madis-Karli Koppel on 20.11.2017.
 * CumulocityController that handles all cumulocity operations from registering sensors to sending data over HTTP
 */

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import ut.madp.cumulocitysensorstreaming.network.NetworkUtil;

public class CumulocityController {

    private static final int UPDATE_DELAY = 500;

    private static final String TAG = "CumulocityController";

    private Set<CumulocityControllerListener> listeners;

    private boolean registered;

    private String auth;

    private String deviceID;

    private ThreadPoolExecutor threadPoolExecutor;

    private BlockingQueue<Runnable> sensorUpdates;

    private ContextWrapper contextWrapper;

    private String versionNumber;

    // Init the controller and create ThreadPoolExecutor that is used to send data over HTTP
    public CumulocityController(ContextWrapper contextWrapper, String versionNumber) {
        this.contextWrapper = contextWrapper;
        this.versionNumber = versionNumber;

        listeners = new HashSet<>();
        registered = false;

        // Init ThreadPoolExecutor that is used to send data over HTTP
        // This pool size is okay for 2000 updates per 10 seconds (200 updates per second)
        int poolSize = 1000000 / UPDATE_DELAY;
        sensorUpdates = new LinkedBlockingQueue<>();
        threadPoolExecutor = new ThreadPoolExecutor(10, poolSize,
                10, TimeUnit.SECONDS, sensorUpdates);
    }

    // Check if we have already registered the device (on second or n'th run)
    // Or request credentials and register the device (on first run)
    public void registerDevice(final String id) {

        // first check if we are registered
        // We write the credentials into preferences when we register
        String credentials = readFromPreferences("com.madp.credentials");

        // Or if we are not registered then try requesting credentials until user accepts them
        // If we receive the credentials then RegistrationHelper starts the registration loop
        //      that creates and registeres the device
        if (credentials.equals("No Credentials")) {
            threadPoolExecutor.execute(new Runnable() {
                public void run() {
                    Log.i(TAG, "Registration loop");
                    boolean tryRegistration = true;
                    while (tryRegistration) {
                        RegistrationHelper registrationHelper = new RegistrationHelper(id);

                        String response = registrationHelper.registerDevice();

                        if (response.length() != 0) {

                            auth = response;
                            deviceID = NetworkUtil.getDeviceId();

                            writeToPreferences("com.madp.credentials", auth);
                            writeToPreferences("com.madp.deviceID", NetworkUtil.getDeviceId());

                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    onDeviceRegistered();
                                }
                            };
                            Log.v(TAG, "register device ok");
                            runnable.run();
                            tryRegistration = false;
                        } else {
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    onDeviceRegistrationError("Have you entered the device serial to Cumulocity? \nDo you have internet connection? \nHave you accepted the registration in Cumulocity?");
                                }
                            };
                            Log.e(TAG, "register device not ok");
                            runnable.run();
                        }
                    }
                }
            });
        } else {
            auth = credentials;
            deviceID = readFromPreferences("com.madp.deviceID");

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    onDeviceRegistered();
                }
            };

            Log.v(TAG, "Device already registered");
            Log.d(TAG, auth);

            runnable.run();
        }
    }

    // Register Cumulocity Smartrest template for given sensor
    // Returns templatename (templateId in sensor implementation, X-id in smartRest)
    // so that sensor can use it when sending data using smartrest
    public String registerSensor(CumulocitySensor sensor) {
        String APP_VERSION = versionNumber;
        String sensorName = sensor.getSensorName();
        String[] names = sensor.getNamesOfValues();

        Log.v(TAG, "registerSensor");
        Log.d(TAG, auth);
        String templateName = "madp_android_" + sensorName.toLowerCase() + "_" + APP_VERSION;

        final String templateContents = generateSmartrestRequest(sensorName, names);

        final String finalTemplateName = templateName;

        threadPoolExecutor.execute(new Runnable() {
            public void run() {
                NetworkUtil.registerSmartTemplate(finalTemplateName, templateContents, auth);
            }
        });

        Log.v(TAG, templateName);
        Log.v(TAG, templateContents);
        return templateName;
    }

    // Uses the ThreadPoolExecutor initialized with this controller to send sensor data
    // Every cumulocity sensor calls this method when they have new data to send
    // Data is sent using smartrest
    public void sendSensorData(final CumulocitySensor sensor, final String[] data) {
        sensor.getSensorName();
        sensor.getTemplateId();

        String dataString = "";

        for (int i = 0; i < data.length; i++) {
            dataString += String.valueOf(data[i]) + ",";
        }

        final String finalDataString = dataString;
        threadPoolExecutor.execute(new Runnable() {
            public void run() {
                NetworkUtil.sendSmartData(sensor.getTemplateId(), finalDataString, auth, deviceID);
            }
        });

        Log.v("cumcontroller", "Sending sensor data for " + sensor.getSensorName() + ": " + Arrays.asList(data).toString());
    }


    // Methods for registering listeners
    // And sending data to the listeners
    public void addListener(CumulocityControllerListener listener) {
        Log.i(TAG, "addListener");
        listeners.add(listener);
    }

    private void onDeviceRegistered() {
        registered = true;
        for (CumulocityControllerListener listener : listeners) {
            listener.onDeviceRegistered();
        }
    }

    private void onDeviceRegistrationError(String error) {
        for (CumulocityControllerListener listener : listeners) {
            listener.onDeviceRegistrationError(error);
        }
    }

    private void onOperation(String operation) {
        for (CumulocityControllerListener listener : listeners) {
            listener.onOperation(operation);
        }
    }
    // END methods for listeners

    // Generates a Smart REST template
    // Called for each sensor
    // Smartrest templates help us save bandwidth and only send
    // templateID, value1, value2,... instead of one big Json
    private String generateSmartrestRequest(String sensorName, String[] names) {
        String templateContents = "10,200,POST,/measurement/measurements,application/vnd.com.nsn.cumulocity.measurement+json,application/vnd.com.nsn.cumulocity.measurement+json,&&,";

        for (String n : names) {
            templateContents += "NUMBER ";
        }

        // NOW is the timestamp, unsigned is the deviceID
        templateContents += "NOW UNSIGNED,\"{\"\"";

        templateContents += sensorName;
        templateContents += "\"\":{";

        for (int i = 0; i < names.length - 1; i++) {
            templateContents += "\"\"";
            templateContents += names[i];
            templateContents += "\"\":{\"\"value\"\":&&},";
        }

        templateContents += "\"\"";
        templateContents += names[names.length - 1];
        templateContents += "\"\":{\"\"value\"\":&&}},";

        templateContents += "\"\"time\"\":\"\"&&\"\",\"\"source\"\":{\"\"id\"\":\"\"&&\"\"},\"\"type\"\":\"\"";
        templateContents += sensorName;
        templateContents += "\"\"}\"";

        return templateContents;
    }

    // Starts listening for operations
    // For that first sends all the operations that device supports to Cumulocity
    // And then subscribes to all operations and starts waiting for them
    // Only executes operations that it is initialized for
    // Works with while loop on separate thread and as such can receive multiple operations
    public void initOperationListener(final List<String> operations) {
        threadPoolExecutor.execute(new Runnable() {
            public void run() {
                Log.v(TAG, "init operations");
                // First let's update the device with the operations that we supported
                // this is done here as when the operations are changed after registration
                // then we can still get correct data when asking for device information
                NetworkUtil.updateDeviceOperations(auth, operations, deviceID);


                // Perform handshake - receive a clientID that is used later
                String clientID = NetworkUtil.handshake(auth).replaceAll("\n|\r", "");
                if (clientID.length() == 0) {
                    Log.e(TAG, "Handshake failed when initializing operations");
                    // TODO handshake failed
                }
                Log.v(TAG, clientID);
                Log.v(TAG, auth);

                // Subscribe for operations.
                // We can subscribe for device operations, or this can also be used
                // subscribe for alarms etc, for that change implementation in NetworkUtil
                // this does not get a response but let it be
                String subscribe = NetworkUtil.subscribe(auth, deviceID, clientID);

                Log.v(TAG, subscribe);

                // Loop for getting operations
                // Every time there is an operation we see if we can perform it
                // And after that we attempt to send a response to mark operation as completed
                while (true) {
                    // Listen for operations that we subscribed to earlier
                    String poll = NetworkUtil.longpoll(auth, clientID);
                    Log.v(TAG, poll);

                    // We get operationID and action from the response json
                    String[] operation = parsePollResponse(poll, operations).split("::");

                    if (operation.length > 1) {
                        String operationID = operation[0].replaceAll("\n|\r", "");
                        String action = operation[1];

                        Log.v(TAG, operationID);
                        Log.v(TAG, action);

                        // Check if we can perfrom the operation (if we are listening to it)
                        for (final String o : operations) {
                            // Perform the operation and mark it as done in Cumulocity
                            if (action.equals(o)) {
                                Log.i(TAG, "Received known operation: " + o);
                                Runnable runnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        onOperation(o);
                                    }
                                };
                                runnable.run();
                                NetworkUtil.updateOperation(auth, operationID);
                            }
                        }

                    }
                }
            }
        });
    }

    // Takes json response from longpoll (waiting for operaton)
    // And extracts the operation ID and operation name from it
    // Can be improved to also take other data, such as comments
    private String parsePollResponse(String poll, List<String> operations) {
        String action = "";
        String operationID = "";
        try {
            JSONArray jsonArray = new JSONArray(poll);
            JSONObject data = jsonArray.getJSONObject(0).getJSONObject("data").getJSONObject("data");
            String status = data.getString("status");

            // Only perform action if it is created
            if (status.equals("PENDING")) {
                Log.v(TAG, data.toString());

                for (String o : operations) {
                    try {
                        data.getString(o);
                        action = o;
                    } catch (JSONException e) {
                    }
                }
                operationID = data.getString("id");
            } else {
                action = "empty";
                operationID = "0";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return operationID + "::" + action;
    }

    // HELPER: to write key and value to device preferences
    private void writeToPreferences(String key, String value) {
        SharedPreferences sharedPref = contextWrapper.getSharedPreferences(this.getClass().getName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    // HELPER: to read key value from device preferences
    private String readFromPreferences(String key) {
        SharedPreferences sharedPref = contextWrapper.getSharedPreferences(this.getClass().getName(), Context.MODE_PRIVATE);
        String credentials = sharedPref.getString(key, "No Credentials");
        return credentials;
    }
}
