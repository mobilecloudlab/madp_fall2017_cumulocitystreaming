package ut.madp.cumulocitysensorstreaming.network;

import android.util.JsonReader;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Madis-Karli Koppel on 24.09.2017.
 * All network communication goes through this class
 */

public class NetworkUtil {

    private static final String TAG = "NetworkUtil";
    private static String DEVICE_ID = "0";
    private static String SITE_URL1 = "https://testing.iot.cs.ut.ee";

    // Register Smartrest template for sending sensor data
    public static void registerSmartTemplate(String id, String contents, String auth) {
        String query = "15," + id + "\n" + contents;

        sendForResponse(auth, null, null, "/s", query, "POST");
    }

    // Send sensor data using smartRest
    // No response as we did not register smartRest template for response
    public static void sendSmartData(String templateHeader, String data, String auth, String deviceID) {
        String query = "15," + templateHeader + "\n";

        query += "200,";
        query += data;
        query += deviceID;

        sendForResponse(auth, null, null, "/s", query, "POST");
    }

    // Smartrest handshake for operations
    // Perform handshake and receive clientID that is used for subscribing to operations
    // and later listening to operations
    public static String handshake(String auth) {
        String query = "80";
        return sendForResponse(auth, "X-Id", "operations", "/devicecontrol/notifications", query, "POST");
    }

    // Subscribe to operations for deviceID
    // When an operation is sent to device then cumulocity also notifies us if we are listening
    public static String subscribe(String auth, String deviceID, String clientID) {

        JSONArray query = new JSONArray();
        JSONObject contents = new JSONObject();
        try {
            contents.put("channel", "/meta/subscribe");
            contents.put("subscription", "/operations/" + deviceID);
            contents.put("clientId", clientID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        query.put(contents);

        return sendForResponse(auth, "Content-Type", "application/json", "/cep/realtime", query.toString(), "POST");
    }

    // Listen for operations that we subscribed to in subscribe()
    // Uses clientID that we received from handshake
    public static String longpoll(String auth, String clientID) {
        JSONArray query = new JSONArray();
        JSONObject contents = new JSONObject();
        try {
            contents.put("channel", "/meta/connect");
            contents.put("connectionType", "long-polling");
            contents.put("clientId", clientID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        query.put(contents);

        return sendForResponse(auth, "Content-Type", "application/json", "/cep/realtime", query.toString(), "POST");
    }

    // Mark operation as successful
    // Sent after we have performed the operation
    public static void updateOperation(String auth, String operationID) {
        JSONObject query = new JSONObject();
        try {
            query.put("status", "SUCCESSFUL");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendForResponse(auth, "Content-Type", "application/json", "/devicecontrol/operations/" + operationID, query.toString(), "PUT");
    }

    // sends a PUT request to Cumulocity to set c8y_SupportedOperations for device
    public static void updateDeviceOperations(String auth, List<String> operationsList, String deviceID) {

        JSONObject body = new JSONObject();
        JSONArray operations = new JSONArray();
        for(String op: operationsList){
            operations.put(op);
        }

        try {
            body.put("c8y_SupportedOperations", operations);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String rep = sendForResponse(auth, "Content-Type", "application/json", "/inventory/managedObjects/" + deviceID, body.toString(), "PUT");
        Log.e("OPERATIONS", rep);
    }

    // Request credentials for device
    // Not for this step we do require credentials that should be the same for every cumulocity instance
    // However these could be changed, so make sure these are the correct ones
    public static Map<String, String> requestCredentials(String id) {
        Log.i(TAG, "Begin credential request");

        Map<String, String> out = new HashMap();

        JSONObject body = new JSONObject();
        try {
            body.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpsURLConnection connection = post("/devicecontrol/deviceCredentials", body, SITE_URL1, "Basic bWFuYWdlbWVudC9kZXZpY2Vib290c3RyYXA6RmhkdDFiYjFm");

        InputStream inputStream;
        try {
            inputStream = connection.getInputStream();

            InputStreamReader responseBodyReader =
                    new InputStreamReader(inputStream, "UTF-8");

            JsonReader jsonReader = new JsonReader(responseBodyReader);

            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String key = jsonReader.nextName();
                String value = jsonReader.nextString();

                Log.v(TAG, key + ":" + value);
                out.put(key, value);
            }
            jsonReader.close();

        } catch (IOException e) {
        }
        return out;
    }

    // https://www.cumulocity.com/guides/rest/device-integration/ step 1
    public static String performPOSTDeviceRegistration(String c8y_hardware_serial, String id, String url, String auth) {
        Log.i(TAG, "Begin POST registration request");

        try {
            // Create the device
            JSONObject body = new JSONObject();
            body.put("externalId", c8y_hardware_serial);
            body.put("type", "c8y_Serial");

            post("/identity/globalIds/" + id + "/externalIds", body, url, auth);

            return null;


        } catch (JSONException e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }

    // https://www.cumulocity.com/guides/rest/device-integration/ step 2
    public static int performPOSTDeviceCreation(String c8y_hardware_name, String c8y_hardware_serial, String c8y_hardware_revision, String url, String auth) {
        Log.i(TAG, "Begin POST creation request");

        try {
            // Create the device
            JSONObject body = new JSONObject();
            body.put("name", "phone " + c8y_hardware_name + " " + c8y_hardware_serial);
            body.put("type", "c8y_SensorPhone");

            body.put("c8y_IsDevice", "{}");

            // Required for operations
            body.put("com_cumulocity_model_Agent", "{}");


            JSONObject hardware = new JSONObject();
            hardware.put("model", c8y_hardware_name);
            hardware.put("serialNumber", c8y_hardware_serial);
            hardware.put("revision", c8y_hardware_revision);

            body.put("c8y_Hardware", hardware);

            HttpsURLConnection connection = post("/inventory/managedObjects", body, url, auth);


            InputStream inputStream = connection.getInputStream();
            InputStreamReader responseBodyReader =
                    new InputStreamReader(inputStream, "UTF-8");

            JsonReader jsonReader = new JsonReader(responseBodyReader);

            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String key = jsonReader.nextName();
                if (key.equals("id")) {
                    String value = jsonReader.nextString();
                    DEVICE_ID = value;
                    return Integer.valueOf(value);
                } else {
                    jsonReader.skipValue();
                }
            }
            jsonReader.close();

            return -1;

        } catch (IOException e) {
            //TODO handle
            Log.e(TAG, Log.getStackTraceString(e));
            return -1;
        } catch (JSONException e) {
            //TODO handle
            Log.e(TAG, Log.getStackTraceString(e));
            return -1;
        }
    }

    // HELPER: A very general method for sending data
    // Should have auth header, prefix(cumulocityurl/prefix), contents in string format and method(POST, PUT)
    // second header is not needed and will be skipped if null
    // returns HTTP response body as string
    private static String sendForResponse(String auth, String header, String headervalue, String prefix, String contents, String method) {
        String response = "";
        String query = contents;
        HttpsURLConnection connection = null;
        try {
            URL url = new URL(SITE_URL1 + prefix);
            Log.v(TAG, "sending to " + SITE_URL1 + prefix);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", auth);
            if (header != null) connection.setRequestProperty(header, headervalue);

            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.setConnectTimeout(100000);

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(query.getBytes());
            outputStream.flush();
            outputStream.close();

            Log.i(TAG, "sent " + String.valueOf(query.toString()));


            InputStream errorStream = connection.getInputStream();
            InputStreamReader responseBodyReader =
                    new InputStreamReader(errorStream, "UTF-8");

            Scanner s = new Scanner(responseBodyReader).useDelimiter("\\A");
            response = s.hasNext() ? s.next() : "";
            s.close();

            Log.v(TAG, "response " + response);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static String getDeviceId() {
        return DEVICE_ID;
    }

    // HELPER: Post to URL and return the connection
    public static HttpsURLConnection post(String urlPostfix, JSONObject body, String SITE_URL, String auth) {
        HttpsURLConnection connection = null;
        try {
            URL url = new URL(SITE_URL + urlPostfix);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", auth);

            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(body.toString().getBytes());
            outputStream.flush();
            outputStream.close();

            // Get the response
            Log.i(TAG, "sent " + String.valueOf(body.toString()));
            Log.i(TAG, "response " + String.valueOf(connection.getResponseCode()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return connection;
    }

    // HELPER: Get jsonobject from stream (stream is from connection)
    private static JSONObject parseInputStream(BufferedReader stream) {
        Log.d(TAG, "parseIsRegisteredRequest");
        String content = "";
        JSONObject jsonObject = null;
        try {
            String decodedString;

            while ((decodedString = stream.readLine()) != null) {
                content += decodedString;
            }

            jsonObject = new JSONObject(content);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    // HELPER: Get jsonobject from stream (stream is from connection)
    // Error stream is a bit different from inputstream and requires different handling
    private static JSONObject parseErrorStream(BufferedReader stream) {
        Log.d(TAG, "parseConnectionResponse");
        JsonReader jsonReader = new JsonReader(stream);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonReader.beginObject();
            while (jsonReader.hasNext()) {

                String key = jsonReader.nextName();
                String value = jsonReader.nextString();
                jsonObject.put(key, value);
            }
            jsonReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    // HELPER: GET only the response code. Used to check device registration status
    public static int performGETRequestForResponseCode(String resourceURL, String SITE_URL, String auth) {
        Log.i(TAG, "Begin GET isRegistered request");

        try {
            URL url = new URL(String.format(SITE_URL + resourceURL));

            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", auth);

            Log.i(TAG, "response " + String.valueOf(connection.getResponseCode()));

            if (connection.getResponseCode() == 200) {
                BufferedReader stream = new BufferedReader(
                        new InputStreamReader(
                                connection.getInputStream()));
                JSONObject responseData = parseInputStream(stream);
                DEVICE_ID = responseData.getJSONObject("managedObject").getString("id");
                Log.d(TAG, responseData.toString());
                return 200;
            } else {
                //This means that device is not registered or there was an issue
                BufferedReader stream = new BufferedReader(
                        new InputStreamReader(
                                connection.getErrorStream()));

                JSONObject responseData = parseErrorStream(stream);
                Log.d(TAG, responseData.toString());

                if (responseData.has("error")) {
                    if (responseData.getString("error").equals("identity/Not Found")) {
                        return 808;
                    }
                }

                return connection.getResponseCode(); //SOME ISSUE WITH PAGE
            }
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
            e.printStackTrace();
            return -1;
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

}
