package ut.madp.cumulocitysensorstreaming;

/**
 * Created by Madis-Karli Koppel on 20.11.2017.
 * Used in ut.madp.SensorActivity and CumulocityController
 * This has three main listeners that handle
 * Device registration success
 * Device registration failure
 * Receiving and Operation
 */

public interface CumulocityControllerListener {

    void onDeviceRegistered();

    void onDeviceRegistrationError(String errorMessage);

    void onOperation(String operation);
}
