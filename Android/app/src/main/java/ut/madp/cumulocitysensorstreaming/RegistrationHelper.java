package ut.madp.cumulocitysensorstreaming;

import android.os.Build;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import ut.madp.cumulocitysensorstreaming.network.NetworkUtil;

/**
 * Created by Madis-Karli Koppel on 20.11.2017.
 * Helper for registering a device in Cumulocity
 * Steps covered:
 * Request credentials for device
 * Steps 1 - 3 from https://www.cumulocity.com/guides/rest/device-integration/
 */

public class RegistrationHelper {

    private String id;

    private String TAG = "Registration_Helper";

    private String SITE_URL = "https://testing.iot.cs.ut.ee";

    private String deviceIdString;

    private String auth;

    public RegistrationHelper(String id){
        this.id = id;
    }

    // Request unique credentials for device and
    // if it receives them then it starts the registrationCycle:
    //      checks if device is registered
    //          based on it registers and creates device
    // returns string that can be used in Authorization header
    public String registerDevice(){
        Log.v(TAG, "checking for credentials");
        Map<String, String> credentials = NetworkUtil.requestCredentials(id);
        Log.v(TAG, "response " + credentials.toString());
        if(credentials.size() > 0){
            String authToken = getBase64(credentials.get("username"), credentials.get("password"));
            auth = authToken;
            registrationCycle();
            return auth;
        }

        return "";
    }

    // Base64 encode username:password, return base64 Authorization header
    private String getBase64(String username, String password) {
        String message = username + ":" + password;

        try {
            byte[] data = message.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            return "Basic " + base64.replace("\n", "");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Password encoding failed");
            e.printStackTrace();
        }

        return "";
    }

    // Check if the device is registered and if not then ... register the device.
    protected void registrationCycle() {
        boolean isRegistered = isDeviceRegistered();

        if (isRegistered) {
            deviceIdString = NetworkUtil.getDeviceId();
        } else {
            createDevice();
        }

    }

    // Following steps from https://www.cumulocity.com/guides/rest/device-integration/
    // STEP 1: CHECK IF THE DEVICE IS ALREADY REGISTERED
    private boolean isDeviceRegistered() {
        Log.i(TAG, "Checking if device is registered");
        // Following two lines are needed for rest test
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String c8y_hardware_serial = Build.SERIAL;
        int response = NetworkUtil.performGETRequestForResponseCode("/identity/externalIds/c8y_Serial/" + c8y_hardware_serial, SITE_URL, auth);

        switch (response) {
            case 200:
                Log.i(TAG, "already registered");
                return true;
            case 401:
                Log.e(TAG, "wrong credentials");
                break;
            case 404:
                Log.e(TAG, "wrong URL");
                break;
            case 808:
                Log.i(TAG, "device not registered");
                return false;
            default:
                Log.e(TAG, "unexpected result in registration check, result:" + response);
        }
        return false;
    }

    // STEP 2: CREATE THE DEVICE IN THE INVENTORY
    private void createDevice() {
        String c8y_hardware_name = Build.MODEL;
        String c8y_hardware_serial = Build.SERIAL;
        String c8y_hardware_revision = Build.VERSION.RELEASE;

        int deviceId = -1;

        try {
            deviceId = NetworkUtil.performPOSTDeviceCreation(c8y_hardware_name, c8y_hardware_serial,
                    c8y_hardware_revision, SITE_URL, auth);
        } catch (NullPointerException e) {
            Log.e(TAG, "Error with device registration");
            e.printStackTrace();
        }


        if (deviceId != -1) {
            // STEP 3: REGISTER THE DEVICE
            Log.i(TAG, "Created device with id:" + deviceId);
            deviceIdString = String.valueOf(deviceId);
            NetworkUtil.performPOSTDeviceRegistration(c8y_hardware_serial, deviceIdString, SITE_URL, auth);
        } else {
            //TODO bad reg
            Log.e(TAG, "Error with device registration");

        }

    }
}
