package ut.madp.cumulocitysensorstreaming;

/**
 * Created by Madis-Karli Koppel on 19.11.2017.
 * Software Module approach for Cumulocity Sensor
 * Declare an interface that supports registering sensors
 * Sending sensor data
 * For implementation see CumulocitySensorImpl
 */

public interface CumulocitySensor {

    void register(CumulocityController controller, String sensorName, String[] namesOfValues, int minimalUpdateTimeMs);

    void pushNewData(String[] data);

    String getSensorName();

    String[] getNamesOfValues();

    String getTemplateId();

}
