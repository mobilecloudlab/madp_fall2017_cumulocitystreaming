package ut.madp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.io.Serializable;

import madp.ut.cumulocitysensordemo.R;



/* Demo of how to use software modules
* It performs steps from registration to sending measurements and receving operations
* User input is only requested in cumulocity side, everything on application side is automatic
* More details when the step is performed
* DO NOTE that it implements SensorEventListener that could also be registered as a separate class
* Steps demoed:
* Requesting device specific credentials
* Registering the device with the credential
* Listening for sensor changes
* Registering Smartrest templates and using them for sending sensor data
* Listening for operations and performing them
* */
public class SensorActivity extends AppCompatActivity implements Serializable {

    private static final String TAG = "Sensor_ACTIVITY";

    private LinearLayout layout;

    private TextView deviceIDText;

    private TextView registrationLog;

    // Messengers for communicating with the service.
    public Handler incomingMessenger = new IncomingHandler();
    Messenger outgoingMessenger = null;

    /** Flag indicating whether we have called bind on the service. */
    boolean mBound;

    // The flow begins here, when Activity is created
    // Only steps that differ from standard view actions are commented
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "Sensor Activity Started");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        layout = (LinearLayout) findViewById(R.id.sensorLayout);

        // Request device hardware serial
        // It is used for requesting device credentials and to identify the device
        final String deviceId = Build.SERIAL;
        deviceIDText = (TextView) findViewById(R.id.deviceID);
        deviceIDText.setText("Device Serial is " + deviceId);

        registrationLog = (TextView) findViewById(R.id.registrationStatus);

        Intent intent = new Intent(this, SensorService.class);
        intent.putExtra("MESSENGER", new Messenger(incomingMessenger));
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        Log.i(TAG, "Sensor Service Intent started");
    }

    // A button for turning sending data on and off
    public void createButton(String sensorName, final int sensorInt){
        Switch sensorSwitch = new Switch(this);
        sensorSwitch.setText(sensorName);
        sensorSwitch.setChecked(true);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(25, 15, 15, 25);
        layoutParams.gravity = Gravity.LEFT;
        sensorSwitch.setLayoutParams(layoutParams);
        layout.addView(sensorSwitch);

        sensorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    sendToService(3, sensorInt, isChecked);

            }
        });
    }

    // Send message to service
    public void sendToService(int what, int arg1, Object body) {
        if (!mBound) {
            Log.e(TAG, "sending message when service is not bound!");
            return;
        }

        Log.i(TAG, "Sending message " + what);
        Message message = Message.obtain();
        switch (what) {
            // Send sensor on/off messages
            case 3 :
                message.what = 3;
                message.arg1 = arg1;
                message.obj = body;
                break;
            default :
                Log.e(TAG, "Attempting to send unknown message");
        }
        try {
            outgoingMessenger.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                // send log updates
                case 1:
                    String text = (String) message.obj;
                    registrationLog.setText(registrationLog.getText() + "\n" + text);
                    break;
                // Send sensor buttons
                case 2:
                    String buttonName = (String) message.obj;
                    createButton(buttonName, message.arg1);
                    break;
                default:
                    Log.e(TAG, "Received unknown message");
            }
        }
    }

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            outgoingMessenger = new Messenger(service);
            mBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            outgoingMessenger = null;
            mBound = false;
        }
    };

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
